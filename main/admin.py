from django.contrib import admin
from main.models.attendance import AttendanceModel
from main.models.event import EventModel, EventRegistrationModel
from main.models.ads import AdsModel
from main.models.models import TrainerTypeModel, TrainerModel, YoutubeVideoLink, StudentQueryFormModel
# Register your models here.


admin.site.register(AttendanceModel)
admin.site.register(EventModel)
admin.site.register(AdsModel)
admin.site.register(TrainerModel)
admin.site.register(TrainerTypeModel)
admin.site.register(YoutubeVideoLink)
admin.site.register(EventRegistrationModel)
admin.site.register(StudentQueryFormModel)
