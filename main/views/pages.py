from django.shortcuts import render
from core.models import SiteModel
from main.models.event import EventModel
from main.models.models import YoutubeVideoLink
from main.forms.enquiry_form import StudentEnquiryForm
from django.http import HttpResponseRedirect
# Create your views here.

def contact_view(request):
    template_name = 'pages/contact.html'
    context = {}
    return render(request, template_name, context)

def events_view(request):
    template_name = 'pages/events.html'
    context = {
        'events':EventModel.objects.all()
    }
    return render(request, template_name, context)


def event_view(request, id):
    template_name = 'pages/event.html'
    context = {
        'event':EventModel.objects.get(id=id)
    }
    return render(request, template_name, context)

def video_view(request):
    template_name = 'pages/video.html'
    context = {
        'videos': YoutubeVideoLink.objects.all(),
    }
    return render(request, template_name, context)

def about_view(request):
    template_name = 'pages/about.html'
    context = {}
    return render(request, template_name, context)

def enquiry_form_view(request):
    template_name = ''
    context = {
        'form' : StudentEnquiryForm(),
    }
    
    if request.method == 'POST':
        form = StudentEnquiryForm(request.POST)
        if form.is_valid:
            return HttpResponseRedirect('/thanks/')
    else:
        return render(request, template_name, context)

def gallery_view(request):
    template_name = 'pages/gallery.html'
    context = {}
    return render(request, template_name, context)

def partner_view(request):
    template_name="pages/partner.html"
    context = {}
    return render(request, template_name, context)

def footer_view(request):
    template_name = 'base/footer.html'
    site = SiteModel.objects.get(name='K-IMA')
    context = {
        'site' : site,
    }
    return render(request, template_name, context)