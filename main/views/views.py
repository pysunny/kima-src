from django.shortcuts import redirect, render
from django.template.response import TemplateResponse
from main.models.models import TrainerModel, YoutubeVideoLink
from main.models.event import EventModel
from main.views.form import ContactForm
# Create your views here.

def index_view(request):
    template_name='base/index.html'
    events = EventModel.objects.all()
    last_event = events.first
    context = {
        'trainers' : TrainerModel.objects.all(),
        'events': EventModel.objects.all(),
        'last_event' : last_event
    }
    return render(request, template_name, context)

def contact_view(request):
    template_name = 'pages/contact.html'
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            form.save()
    else:
        form = ContactForm()
    return render(request, template_name, {'form':form})
