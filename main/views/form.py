from django.forms import ModelForm
from main.models.models import StudentQueryFormModel

class ContactForm(ModelForm):
    class Meta:
        model = StudentQueryFormModel
        fields = '__all__'
