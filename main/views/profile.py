from django.shortcuts import render
from main.models.models import TrainerModel, TrainerTypeModel

def profile_view(request, id):
    template_name = 'profile/dash.html'
    context = {
        'trainer': TrainerModel.objects.get(id=id)
    }
    return render(request, template_name, context)