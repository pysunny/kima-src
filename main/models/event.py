from django.db import models
from django.db.models.fields import BLANK_CHOICE_DASH

class SponserModel(models.Model):
    name = models.CharField(max_length=500, blank=True, null=True)

    def __str__(self):
        return self.name

class EventModel(models.Model):
    name = models.CharField(max_length=500, blank=True, null=True)
    Description = models.TextField(null=True, blank=True)
    organized_by = models.CharField(max_length=500, blank=True, null=True)
    place = models.TextField(blank=True, null=True)
    sponser = models.ForeignKey(SponserModel, on_delete=models.CASCADE, null=True, blank=True)
    manager = models.CharField(max_length=100, blank=True, null=True)
    invest = models.IntegerField(blank=True, null=True)


    def __str__(self) -> str:
        return self.name

class EventRegistrationModel(models.Model):
    SEX_CHOICE = [
        ('M', 'Male'),
        ('F', 'Female')
    ]
    event = models.ForeignKey(EventModel, on_delete=models.CASCADE, null=True, blank=True)
    visitor = models.CharField(max_length=250, null=True, blank=True)
    player_name = models.CharField(max_length=100, null=True, blank=True)
    sex = models.CharField(max_length=10, choices=SEX_CHOICE, null=True, blank=True)
    weight = models.IntegerField(null=True, blank=True)
    dob = models.DateField(null=True, blank=True)
    fee = models.IntegerField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        return self.player_name

