from django.db import models

class AdsModel(models.Model):
    name = models.CharField(max_length=200, null=True, blank=True)
    ads_url = models.URLField(null=True, blank=True)
    ad_target = models.URLField(null=True, blank=True)
    ad_place = models.CharField(max_length=200, null=True, blank=True)
    vendor = models.CharField(max_length=200, null=True, blank=True)
    start_date = models.DateTimeField(null=True, blank=True)
    end_date = models.DateTimeField(null=True, blank=True)
    manager = models.CharField(max_length=200, null=True, blank=True)
    status = models.BooleanField(default=False)

    def __str__(self):
        return self.name