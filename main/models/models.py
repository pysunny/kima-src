from django import db
from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from django.db.models.enums import Choices

# Create your models here.
class TrainerTypeModel(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True, db_index=True)

    def __str__(self) -> str:
        return self.name

class TrainerModel(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True, db_index=True)
    dp = models.URLField(blank=True)
    achievement =RichTextUploadingField(blank=True, null=True)
    bio = RichTextUploadingField(blank=True, null=True)
    type = models.ManyToManyField(TrainerTypeModel, blank=True, null=True)

    def __str__(self) -> str:
        return self.name

class YoutubeVideoLink(models.Model):
    title = models.CharField(max_length=250, null=True, blank=True)
    video_url = models.URLField(null=True, blank=True)
    Description = models.TextField(null=True, blank=True)

    def __str__(self) -> str:
        return self.title

class StudentQueryFormModel(models.Model):
    SEX_CHOICE = [
        ('M', 'Male'),
        ('F', 'Female')
    ]
    student_name = models.CharField(max_length=100, null=True, blank=True, db_index=True)
    address = models.TextField(null=True, blank=True)
    age = models.IntegerField(null=True, blank=True)
    gender = models.CharField(max_length=10, choices=SEX_CHOICE, default='Male')
    contact_number = models.BigIntegerField(null=True, blank=True, db_index=True)
    email = models.CharField(max_length=50, null=True, blank=True, db_index=True)
    enquiry_text = models.TextField(null=True, blank=True)
    cretaed_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'StudentQueryFormModel'
    def __str__(self) -> str:
        return self.student_name
