from django import forms
from django.db.models import fields
from main.models.models import StudentQueryFormModel

class StudentEnquiryForm(forms.ModelForm):
    class Meta:
        model = StudentQueryFormModel
        fields = '__all__'
