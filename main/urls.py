from django.urls import path
from main.views.views import index_view
from main.views.pages import contact_view, event_view, events_view, video_view, about_view, gallery_view, partner_view
from main.views.profile import profile_view
urlpatterns = [
    path('',index_view, name='index'),
    path('kima/contact/', contact_view, name='contact'),
    path('kima/events/', events_view, name='events'),
    path('kima/event/<int:id>/', event_view, name='event'),
    path('kima/video/', video_view, name='video'),
    path('kima/about/', about_view, name='about'),
    path('kima/gallery/', gallery_view, name='gallery'),
    path('kima/trainer/<int:id>/', profile_view, name='profile'),
    path('kima/partner-with-us/', partner_view, name="partner-with-us")
    #path('kima/contact/', contact_view, name='contact'),
]
