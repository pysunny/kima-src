from django.db import models
from django.contrib.auth.models import AbstractUser
from django.conf import settings
from django.utils.translation import ugettext_lazy as _


# Create your models here.

class User(AbstractUser):
    email = models.EmailField(_('Email Address'), unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username', 'first_name', 'last_name']

    def __str__(self):
        return "{}".format(self.email)


class UserProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='profile', blank=True)
    title = models.CharField(max_length=5, blank=True, null=True)
    dob = models.DateField(blank=True, null=True)
    mobile = models.CharField(max_length=10, null=True)
    address = models.CharField(max_length=100, blank=True, null=True)
    country = models.CharField(max_length=100, blank=True, null=True)
    city = models.CharField(max_length=100, blank=True, null=True)
    zip = models.CharField(max_length=6, blank=True, null=True)
    photo  = models.ImageField(upload_to = 'uploads', blank=True, null=True)

    def __str__(self):
        return self.user.email

class SiteModel(models.Model):
    name = models.CharField(max_length=100, default='K-IMA')
    logo = models.URLField(null=True, blank=True)
    banner = models.URLField(null=True, blank=True)
    facebook_url = models.URLField(null=True, blank=True)
    pin_url = models.URLField(null=True, blank=True)
    insta_url = models.URLField(null=True, blank=True)

    def __str__(self) -> str:
        return self.name