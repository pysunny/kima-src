from django.contrib import admin
from core.models import User, UserProfile, SiteModel
# Register your models here.

admin.site.register(User)
admin.site.register(UserProfile)
admin.site.register(SiteModel)